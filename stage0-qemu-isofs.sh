#!/usr/bin/sh
# Copyright (C) 2024, csdvrx, MIT licensed

# Cosmopolinux: uses a linux kernel around cosmopolitan binaries, with as little bloat as possible
#
# Can run baremetal, or with qemu: here qemu in UEFI mode with the .iso file

# Have some hugepage ram, because cmdline keeps 256M for the crashkernel
sudo echo 550 > /proc/sys/vm/nr_hugepages

# Minimal form to try ELTO1 BIOS MBR boot with the baremetal/isolinux.cfg cmdline:
#qemu-system-x86_64 -boot d -cdrom cosmopolinux.iso -m 512
# will use kernel options from baremetal/isolinux.cfg
# goal now: good cmdline is isolinux.cfg for eltorito boot

# In theory, a minimal form to try ELTO2 UEFI boot needs both:
#  - the OVMF code (firmware): secureboot enabled on OVMF_CODE.secboot.fd 
# will need to enter the firmware setup menu, like with `fwsetup` during grub
#cf https://www.linux.it/~ema/posts/efi-sb-notes/
#will need to check signatures: `sbverify --list`and check support:
# dmesg | grep -i "secure.*boot"
#  - the EFIVAR local copy (non-volatile variable store)
#cp /usr/share/edk2/x64/OVMF_VARS.4m.fd /tmp/flash1_OVMF_VARS.4m.fd
#UEFI=" -bios /usr/share/edk2/x64/OVMF.4m.fd \
#-drive if=pflash,media=disk,id=drive0,cache=writethrough,format=raw,readonly=on,file=/usr/share/edk2/x64/OVMF_CODE.4m.fd \
#-drive if=pflash,media=disk,id=drive1,cache=writethrough,format=raw,file=/tmp/flash1_OVMF_VARS.fd"
#
# If the OVMF file is missing, UEFI is also available from linaro:
#cf https://gist.github.com/thalamus/561d028ff5b66310fac1224f3d023c12
#cf https://gist.github.com/mdeanda/b11ba916fb93c9cd438b94c86f03e004
# Get their QEMU_EFI.fd and use it to create 2 files to use with the if=pflash:
#ls QEMU_EFI.fd 2> /dev/null || curl -LO https://releases.linaro.org/components/kernel/uefi-linaro/latest/release/qemu64/QEMU_EFI.fd
#dd if=/dev/zero of=flash0.img bs=1M count=64
#dd if=QEMU_EFI.fd of=flash0.img conv=notrunc
#dd if=/dev/zero of=flash1.img bs=1M count=64

# Can be even more minimalistic by using whatever the defaults EFIVARs are:
UEFI=" -bios /usr/share/edk2/x64/OVMF_CODE.4m.fd \
-drive if=pflash,media=disk,id=drive0,cache=writethrough,format=raw,readonly=on,file=/usr/share/edk2/x64/OVMF_CODE.4m.fd "

# Whether using secureboot or not, provide a crypto dev:
CRYP=" -object cryptodev-backend-builtin,id=cryptodev0 -device virtio-crypto-pci,id=crypto0,cryptodev=cryptodev0"

## Then:
# can pretend the ISO was written to a CD
CDROM="-boot d -cdrom cosmopolinux.iso"
# index=1: primary,master
# index=2: primary,slave
# index=3: secondary,master
# index=4: secondary,slave
# so can be more specific if needed:
#CDROM="-drive file=cosmopolinux.iso,media=cdrom,if=ide,index=2 -boot d"

# can pretend the ISO was written to a EHCI usb 2.0 thumbdrive
THUMB="-drive file=cosmopolinux.iso,format=raw,if=none,id=thumb -device usb-ehci,id=ehci -device usb-storage,drive=thumb,bus=ehci.0"
# DRIVE is either CDROM or THUMB for the ISO tests
#DRIVE="${THUMB}"
DRIVE="${CDROM}"

# connect the qemu monitor to telnet port 6999
MONIT="-monitor telnet:127.0.0.1:6999,server,nowait"
# connect stdio to ttyS0 for extra debug
STDIO="-chardev stdio,id=stdinout -serial chardev:stdinout"
# connect stdio to hvc0
#STDIO="-chardev stdio,id=stdinout -device virtio-serial -device virtconsole,chardev=stdinout"
# connect telnet port 7000 to ttyS1 for more debug
P7000="-serial telnet:localhost:7000,server,nowait,nodelay"
# connect a pty from /dev/pts/X to hvc0
PTSTX="-chardev pty,id=pty0 -device virtio-serial-pci -device virtconsole,chardev=pty0"

# On linux, accel=kvm is better, but can use the generic accel=hvm
# On apple, can use accel=hvf
qemu-system-x86_64 \
-enable-kvm \
-cpu host -smp cores=2 \
-machine type=pc,accel=kvm -rtc base=localtime -action reboot=shutdown \
${UEFI} ${CRYPT} \
${DRIVE} \
-m 512 -mem-path /dev/hugepages \
-usb -usbdevice tablet \
${MONIT} \
${STDIO} \
${P7000} \
${PTSTX} \
-display vnc=:0

# Display can be either through a gtk window that automatically opens:
#-vga virtio -display gtk,full-screen=off,gl=off,grab-on-hover=on
# Or through a vnc server that will wait for connections on port 5900:
#-display vnc=:0

## WARNING: missing efivars, but can check their defaults after they timeout:
#BdsDxe: failed to load Boot0001 "UEFI QEMU DVD-ROM QM00003 " from PciRoot(0x0)/Pci(0x1,0x1)/Ata(Secondary,Master,0x0): Not Found
#BdsDxe: failed to load Boot0002 "UEFI PXEv4 (MAC:525400123456)" from PciRoot(0x0)/Pci(0x3,0x0)/MAC(525400123456,0x1)/IPv4(0.0.0.0,0x0,DHCP,0.0.0.0,0.0.0.0,0.0.0.0): Not Found
#UEFI Interactive Shell v2.2
#EDK II
#UEFI v2.70 (EDK II, 0x00010000)
#Mapping table
#      FS0: Alias(s):CD0c1:;BLK2:
#          PciRoot(0x0)/Pci(0x1,0x1)/Ata(0x0)/CDROM(0x1)
#     BLK0: Alias(s):
#          PciRoot(0x0)/Pci(0x1,0x1)/Ata(0x0)
#0x1)/Ata(0 OM(0x0)"
#Press ESC in 5 seconds to skip startup.nsh or any other key to continue.
#Shell> 
## There:
# can check maps: 
#Shell> maps
# can dump boot entries
#Shell> bcfg boot dump -v
# If want to add an entry:
#Shell> bcfg boot add 0 fs0:\cosmopolinux.efi "Cosmopolinux"
# Can check actual efivars, like coreboot:
#Shell> dmpstore SecureBoot
