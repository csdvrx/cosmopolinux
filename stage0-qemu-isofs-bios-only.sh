#!/usr/bin/sh

# Have some hugepage ram, because keeps 256M for the crashkernel in  cmdline
sudo echo 550 > /proc/sys/vm/nr_hugepages

## Minimal form to try ELTO1 bios boot with the baremetal/isolinux.cfg cmdline:
#qemu-system-x86_64 -boot d -cdrom cosmopolinux.iso -m 512

## Can then try next also adding a hard drive and a serial port, like:
#qemu-system-x86_64 -boot d -cdrom cosmopolinux.iso -m 512 \
# -chardev stdio,id=stdinout -serial chardev:stdinout
# -drive if=virtio,file=cosmopolinux.ntfs3,format=raw,cache.direct=on,aio=native,media=disk -boot c

## A less minimal version is:
qemu-system-x86_64 \
-enable-kvm \
-cpu host -smp cores=2 \
-machine type=pc,accel=kvm -rtc base=localtime -action reboot=shutdown \
-boot d -cdrom cosmopolinux.iso \
-m 512 -mem-path /dev/hugepages \
-usb -usbdevice tablet \
-monitor telnet:127.0.0.1:6999,server,nowait \
-chardev stdio,id=stdinout -serial chardev:stdinout \
-serial telnet:localhost:7000,server,nowait,nodelay \
-chardev pty,id=pty0 -device virtio-serial-pci -device virtconsole,chardev=pty0 \
-display vnc=:0

## To automatically start the gtk GUI, replace the last line with:
#-vga virtio -display gtk,full-screen=off,gl=off,grab-on-hover=on
# Or just connect to the vnc instance with: `gvncviewer ::1:5900`
